const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const mysql = require('mysql')
var cors = require('cors')

app.use(cors())
app.use(bodyParser.json())

const conn = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'restapi_test'
    })


app.get('/api/',(req,res) => {
    conn.query('SELECT * FROM `members`',(err,result) => {
        if(err){
            return res.json(err)
        }else{
            return res.json(result)
        }
    })
})

app.post('/api/add',(req,res) => {
   
    conn.query('INSERT INTO `members` (`product`, `price`) VALUES (?,?)',[req.body.name,req.body.price])
})

app.delete('/api/:id',(req,res) => {
    
    conn.query('DELETE FROM `members` WHERE members.id = ?',[req.params.id])
})

app.put('/api/:id',(req,res) => {
    
    conn.query('UPDATE `members` SET `product` = ?, `price` = ? WHERE `members`.`id` = ?',[req.body.name,req.body.price,req.params.id])
})

app.listen(3001,() =>{
    console.log('Server started on port 3001...');
    })